import {BOARD_SIZE, CELL_CONFIG, NOBODY, TURNS} from "./Configs/app";
import {getEmptyRowIndex} from "./Helpers/columnHelper";
import {hasWinner, hasFreeCells, createBoardData} from "./Helpers/boardHelper";


const httpServer = require('http').createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write('Hello World!');
    res.end();
});

const io = require("socket.io")(httpServer, {
    cors: {
        origin: "*",
        methods: ["GET", "POST", "DELETE", "PUT"]
    }
});

const rooms = {
    // ['roomName']: {
    //     roomName: 'room1',
    //     players: {
    //         ['playerName']: {playerName: 'p1', color: 'YELLOW'},
    //         ['playerName']: {playerName: 'p1', color: 'RED'}
    //     },
    //     turn: 'PlayerName',
    //     boardData: [],
    //     winner: null,
    //     winnerCellIndeces: [],
    // }
};

io.on("connection", (socket) => {

    socket.on("request-join-room", ({roomName, playerName}, callbackFn) => {
        socket.join(roomName);

        if (!rooms[roomName]) {
            rooms[roomName] = {
                roomName,
                players: {},
                turn: playerName,
                boardData: createBoardData(BOARD_SIZE.COLS, BOARD_SIZE.ROWS),
                winnerCellIndeces: [],
                winner: null,
            }
        }

        const currentRoom = rooms[roomName];

        const playersQty = Object.keys(currentRoom.players).length;

        if (!currentRoom.players[playerName]) {
            if (playersQty >= 2) {
                callbackFn(false, 'Room is full');
                return;
            }

            currentRoom.players[playerName] = {
                playerName,
                color: playersQty === 0 ? TURNS.YELLOW : TURNS.RED,
            }
        }

        socket.to(roomName).emit('room-data', currentRoom);
        socket.emit('room-data', currentRoom)
        callbackFn(true);
    });


    socket.on("request-do-step", ({roomName, columnId}) => {
        let currentRoom = rooms[roomName];
        const currentBoardData = currentRoom.boardData;
        const currentTurnColor = currentRoom.players[currentRoom.turn].color;

        const emptyRowId = getEmptyRowIndex(currentBoardData[columnId]);
        const newBoardData = [
            ...currentBoardData.slice(0, columnId),
            currentBoardData[columnId].map((row, rowId) => {
                if (rowId === emptyRowId) {
                    return CELL_CONFIG[currentTurnColor];
                }
                return row;
            }),
            ...currentBoardData.slice(columnId + 1)
        ]
        currentRoom.boardData = newBoardData;

        const winnerCellIndeces = hasWinner(newBoardData, columnId, emptyRowId);
        if (winnerCellIndeces) {
            const dataToEmit = {
                boardData: newBoardData,
                winner: currentRoom.turn,
                winnerCellIndeces: winnerCellIndeces,
            };
            currentRoom.winner = currentRoom.turn;
            currentRoom.winnerCellIndeces = winnerCellIndeces;

            socket.to(roomName).emit('end-game', dataToEmit);
            socket.emit('end-game', dataToEmit);
        } else if (hasFreeCells(newBoardData)) {
            const newTurnName = Object.keys(currentRoom.players).find(name => name !== currentRoom.turn);
            currentRoom.turn = newTurnName;

            const dataToEmit = {
                boardData: newBoardData,
                turn: newTurnName,
            };
            socket.to(roomName).emit('do-step', dataToEmit);
            socket.emit('do-step', dataToEmit);
        } else {
            const dataToEmit = {
                boardData: newBoardData,
                winner: NOBODY,
                winnerCellIndeces: [],
            };
            currentRoom.winner = NOBODY;
            currentRoom.winnerCellIndeces = [];

            socket.to(roomName).emit('end-game', dataToEmit);
            socket.emit('end-game', dataToEmit);
        }
    });

    socket.on("request-reset-game", ({roomName}) => {
        const currentRoom = rooms[roomName];

        currentRoom.boardData = createBoardData(BOARD_SIZE.COLS, BOARD_SIZE.ROWS);
        currentRoom.winnerCellIndeces = [];
        currentRoom.winner = null;

        socket.to(roomName).emit('room-data', currentRoom);
        socket.emit('room-data', currentRoom)
    });

    socket.on("request-leave-room", ({roomName, playerName}) => {
        const currentRoom = rooms[roomName];
        delete currentRoom.players[playerName];

        socket.to(roomName).emit('room-data', currentRoom);
        socket.emit('room-data', {
            boardData: [],
            players: {},
            winner: null,
            winnerCellIndeces: [],
            turn: null,
        });
    });


    socket.on("request-rooms-list", (callbackFn) => {
        callbackFn(Object.values(rooms));
    });

    socket.on("request-remove-room", ({roomName}, callbackFn) => {
        delete rooms[roomName]
        socket.to(roomName).emit('remove-room', {roomName});
        callbackFn();
    });

});

const port = process.env.PORT || 3001;

httpServer.listen(port);