# React connect four - server

## Local development

Run ```npm install```

Run ```npm start```


## Dockerize it

Run ```docker build -t react-connect-four-server .```

Run ```docker run -p 3001:3001 -e PORT=3001 -d react-connect-four-server```


## Publish to heroku 

#### install heroku cli

See documentation
```https://devcenter.heroku.com/articles/heroku-cli```


#### login to heroku

```heroku login```


#### publish docker image and run container

```heroku container:login```

```heroku container:push web --app=react-connect-four1``` 

```heroku container:release web --app=react-connect-four1```


#### api url

```https://react-connect-four1.herokuapp.com/```