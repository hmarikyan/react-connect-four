import {CELL_CONFIG} from "../Configs/app";

export const getEmptyRowIndex = (column) => {
    let rowId = column.length - 1;

    while (rowId > 0) {
        if (column[rowId] === CELL_CONFIG.EMPTY) {
            break;
        }

        rowId--;
    }

    if (column[rowId] === CELL_CONFIG.EMPTY) {
        return rowId;
    }
    return -1;
}