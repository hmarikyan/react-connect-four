import {CELL_CONFIG, CELLS_TO_CONNECT, TURNS} from "../Configs/app";

export const createBoardData = (cols, rows) => {
    return Array(cols).fill(Array(rows).fill(CELL_CONFIG.EMPTY));
}

export const hasWinner = (boardData, columnId, rowId) => {
    const turnValue = boardData[columnId][rowId];

    let counter = 0;
    let indeces = [];

    let tempColumnId = columnId;
    let tempRowId = rowId;

    /*************    CHECK HORIZONTAL     ************/
    //check horizontal --
    while (tempColumnId >= 0) {
        if (boardData[tempColumnId][rowId] !== turnValue) {
            break;
        }
        counter++
        indeces.push(`${tempColumnId}${rowId}`);
        if (counter >= CELLS_TO_CONNECT) {
            return indeces;
        }
        tempColumnId--;
    }

    //check horizontal ++
    if (columnId + 1 <= boardData.length - 1) {
        tempColumnId = columnId + 1;
        while (tempColumnId <= boardData.length - 1) {
            if (boardData[tempColumnId][rowId] !== turnValue) {
                break;
            }
            counter++
            indeces.push(`${tempColumnId}${rowId}`);
            if (counter >= CELLS_TO_CONNECT) {
                return indeces;
            }
            tempColumnId++;
        }
    }
    /*************  END CHECK HORIZONTAL     ************/


    /*************    CHECK PRIMARY DIAGONAL   ************/
    counter = 0;
    indeces = [];

    // check primary diagonal --
    tempColumnId = columnId;
    tempRowId = rowId;
    while (tempColumnId >= 0 && tempRowId >= 0) {
        if (boardData[tempColumnId][tempRowId] !== turnValue) {
            break;
        }
        counter++
        indeces.push(`${tempColumnId}${tempRowId}`);
        if (counter >= CELLS_TO_CONNECT) {
            return indeces;
        }
        tempColumnId--;
        tempRowId--;
    }

    // check primary diagonal ++
    if (columnId + 1 <= boardData.length - 1 && rowId + 1 <= boardData[0].length - 1) {
        tempColumnId = columnId + 1;
        tempRowId = rowId + 1;
        while (tempColumnId <= boardData.length - 1 && tempRowId <= boardData[0].length - 1) {
            if (boardData[tempColumnId][tempRowId] !== turnValue) {
                break;
            }
            counter++
            indeces.push(`${tempColumnId}${tempRowId}`);
            if (counter >= CELLS_TO_CONNECT) {
                return indeces;
            }
            tempColumnId++;
            tempRowId++;
        }
    }
    /*************  END CHECK PRIMARY DIAGONAL   ************/


    /*************   CHECK SECONDARY DIAGONAL   ************/
    counter = 0;
    indeces = [];

    // check secondary diagonal -- (col)
    tempColumnId = columnId;
    tempRowId = rowId;
    while (tempColumnId >= 0 && tempRowId <= boardData[0].length - 1) {
        if (boardData[tempColumnId][tempRowId] !== turnValue) {
            break;
        }
        counter++
        indeces.push(`${tempColumnId}${tempRowId}`);
        if (counter >= CELLS_TO_CONNECT) {
            return indeces;
        }
        tempColumnId--;
        tempRowId++;
    }

    // check secondary diagonal ++ (col)
    if (columnId + 1 <= boardData.length - 1 && rowId - 1 >= 0) {
        tempColumnId = columnId + 1;
        tempRowId = rowId - 1;
        while (tempColumnId <= boardData.length - 1 && tempRowId >= 0) {
            if (boardData[tempColumnId][tempRowId] !== turnValue) {
                break;
            }
            counter++
            indeces.push(`${tempColumnId}${tempRowId}`);
            if (counter >= CELLS_TO_CONNECT) {
                return indeces;
            }
            tempColumnId++;
            tempRowId--;
        }
    }
    /*************  END CHECK SECONDARY DIAGONAL   ************/


    /*************  CHECK VERTICAL   ************/
    counter = 0;
    indeces = [];
    tempRowId = rowId;
    while (tempRowId <= boardData[0].length - 1) {
        if (boardData[columnId][tempRowId] !== turnValue) {
            break;
        }
        counter++
        indeces.push(`${columnId}${tempRowId}`);
        if (counter >= CELLS_TO_CONNECT) {
            return indeces;
        }
        tempRowId++;
    }
    /*************  end CHECK VERTICAL   ************/

    return null;
}

export const hasFreeCells = (boardData) => {
    return boardData.some(col => col[0] === CELL_CONFIG.EMPTY);
}