import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import './App.css';
import Board from "./Board/Board";
import Setup from "./Setup/Setup";
import RoomsList from "./RoomsList/RoomsList";


function App() {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route path="/game">
                        <Board />
                    </Route>
                    <Route path="/rooms-list/123456">
                        <RoomsList />
                    </Route>
                    <Route path="/" exact>
                        <Setup />
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
