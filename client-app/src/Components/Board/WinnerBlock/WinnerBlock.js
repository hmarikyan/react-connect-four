import PropTypes from 'prop-types';

import './WinnerBlock.css';
import Button from "../../Common/Button/Button";

const WinnerBlock = ({winner, isWinnerYou, resetBoard}) => {
    return <div className={'WinnerBlock'}>
        <div>
            <h2 className={'WinnerBlock__Title ' + (isWinnerYou ? 'GREEN': 'RED')}>
                {
                    winner === 'NOBODY'
                        ? 'NOBODY won'
                        : isWinnerYou ? 'You are winner' : 'You are loser'
                }
            </h2>
        </div>
        <div className={'WinnerBlock__Button'}>
            <Button onClick={resetBoard}>Reset board</Button>
        </div>
    </div>
}

WinnerBlock.propTypes = {
    winner: PropTypes.string,
    isWinnerYou: PropTypes.bool,
    resetBoard: PropTypes.func.isRequired,
}

export default WinnerBlock;