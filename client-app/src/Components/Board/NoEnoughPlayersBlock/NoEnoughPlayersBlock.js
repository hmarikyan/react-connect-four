import PropTypes from 'prop-types';

import './NoEnoughPlayersBlock.css';
import Button from "../../Common/Button/Button";

const NoEnoughPlayersBlock = ({leaveRoom}) => {
    return <div className={'NoEnoughPlayersBlock'}>
        <div>
            <h2>No enough players</h2>
        </div>
        <div>
            Waiting...
        </div>
        <div className={'NoEnoughPlayersBlock__Button'}>
            <Button onClick={leaveRoom}>Leave room</Button>
        </div>
    </div>
}

NoEnoughPlayersBlock.propTypes = {
    leaveRoom: PropTypes.func.isRequired,
}

export default NoEnoughPlayersBlock;