import PropTypes from 'prop-types';

import './Column.css';
import Cell from "../Cell/Cell";
import {getEmptyRowIndex} from "../../../Helpers/columnHelper";


const Column = ({
                    disabled,
                    column,
                    columnId,
                    onColumnClick,
                    winnerCellIndeces,
                }) => {
    const handleColumnClick = () => {
        if(disabled) {
           return;
        }

        const emptyRowIndex = getEmptyRowIndex(column);
        if (emptyRowIndex >= 0) {
            onColumnClick(columnId, emptyRowIndex);
        }
    }

    return <div
        className={'Column ' + ( disabled? 'disabled':null)}
        onClick={handleColumnClick}
    >
        {column.map((cell, index) => {
            return <Cell
                key={`${columnId}_${index}`}
                cell={cell}
                rowId={index}
                columnId={columnId}
                rowsCount={column.length}
                isWinnerCell={winnerCellIndeces?.includes(`${columnId}${index}`) || false}
            />
        })}
    </div>
}

Column.propTypes = {
    column: PropTypes.arrayOf(
        PropTypes.number
    ),
    columnId: PropTypes.number,
    winner: PropTypes.string,
    winnerCellIndeces: PropTypes.array,

    onColumnClick: PropTypes.func,
}

export default Column;