import PropTypes from 'prop-types';

import './BoardInfo.css';
import {CELLS_TO_CONNECT} from "../../../Configs/app";
import Button from "../../Common/Button/Button";

const BoardInfo = ({
                       myName,
                       roomName,
                       turn,
                       yourColor,
                       isYourTurn,
                       leaveRoom,
                   }) => {
    return <div className={'BoardInfo'}>

        <div className={'BoardInfo__Header'}>
            <div className={'BoardInfo__Header__Left'}>
                <h2>{myName}, you are in {roomName}</h2>
            </div>

            <div className={'BoardInfo__Header__Right'}>
                <Button size={'small'} onClick={() => leaveRoom()}>Leave room</Button>
            </div>
        </div>

        <div className={'BoardInfo__Content'}>
            <h4 className={'BoardInfo__Content__Game ' + yourColor}>
                Connect {CELLS_TO_CONNECT} cells (horizontal, diagonal, vertical) to win.
            </h4>

            <h4 className={'BoardInfo__Content__Turn'}>
               It's {isYourTurn ? 'Your' : 'Opponent\'s'} turn
            </h4>
        </div>
    </div>
}

BoardInfo.propTypes = {
    turn: PropTypes.string,
    isYourTurn: PropTypes.bool,
    leaveRoom: PropTypes.func,
}

export default BoardInfo;