import {useCallback, useContext, useEffect, useRef} from "react";
import {useHistory} from 'react-router-dom';

import './Board.css';
import {TURNS} from "../../Configs/app";
import PlayerCommunicationService from "../../Services/PlayerCommunicationService";
import UserCredentialsStorage from "../../Services/UserCredentialsStorage";
import CommunicationContext from "../../Contexts/CommunicationContext";
import Column from "./Column/Column";
import Overlay from "../Common/Overlay/Overlay";
import BoardInfo from "./BoardInfo/BoardInfo";
import NoEnoughPlayersBlock from "./NoEnoughPlayersBlock/NoEnoughPlayersBlock";
import WinnerBlock from "./WinnerBlock/WinnerBlock";


const Board = () => {
    const history = useHistory();
    const myName = useRef(UserCredentialsStorage.getMyName());
    const {
        players,
        boardData,
        turn: turnName,
        roomName,
        winner,
        winnerCellIndeces,
    } = useContext(CommunicationContext);

    const yourColor = TURNS[players[myName.current]?.color] || null;

    const onColumnClick = useCallback((columnId) => {
        if (winner) {
            return false;
        }

        PlayerCommunicationService.doStepAndSwitchTurn({
            roomName,
            columnId,
        })
    }, [winner, roomName]);

    const resetBoard = useCallback(() => {
        PlayerCommunicationService.resetGame({roomName});
    }, [roomName])

    const leaveRoom = useCallback(async () => {
        await PlayerCommunicationService.leaveRoom({roomName, myName: myName.current});
        UserCredentialsStorage.removeRoomName();

        history.push('/');
    }, [myName.current, roomName]);

    useEffect(() => {
        if (!UserCredentialsStorage.getRoomName()) {
            history.push('/');
        }
    }, [roomName]);

    return <Overlay
        show={!!winner || Object.keys(players).length < 2}
        content={
            Object.keys(players).length < 2 ? <NoEnoughPlayersBlock leaveRoom={leaveRoom} /> :
                winner ? <WinnerBlock winner={winner} isWinnerYou={winner === myName.current} resetBoard={resetBoard}/> : null
        }
    >
        <BoardInfo
            myName={myName.current}
            roomName={roomName}
            turn={turnName}
            yourColor={yourColor}
            isYourTurn={turnName === myName.current}
            leaveRoom={leaveRoom}
        />

        <div className={'Board'}>
            {boardData.map((column, index) => {
                return <Column
                    disabled={myName.current !== turnName}
                    onColumnClick={onColumnClick}
                    key={index}
                    columnId={index}
                    column={column}
                    winner={winner}
                    winnerCellIndeces={winnerCellIndeces}
                />
            })}
        </div>
    </Overlay>
}

export default Board;