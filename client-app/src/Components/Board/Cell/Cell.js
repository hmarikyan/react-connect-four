import PropTypes from 'prop-types';

import './Cell.css';
import {getAnimationDuration, getClassNameByCellValue} from "../../../Helpers/cellHelper";


const Cell = ({
                  cell,
                  columnId,
                  rowId,
                  rowsCount,
                  isWinnerCell,
}) => {
    const innerClassNames = ['Cell__Inner'];
    innerClassNames.push(getClassNameByCellValue(cell));
    if(isWinnerCell) {
        innerClassNames.push('winner')
    }

    return <div className={'Cell'}>
        {cell ? <div
            data-duration={getAnimationDuration(rowId, rowsCount)}
            className={innerClassNames.join(' ')}
        /> : null}
    </div>
}

Cell.propTypes = {
    cell: PropTypes.number,
    columnId: PropTypes.number,
    rowId: PropTypes.number,
    rowsCount: PropTypes.number,
    isWinnerCell: PropTypes.bool,
}

export default Cell;