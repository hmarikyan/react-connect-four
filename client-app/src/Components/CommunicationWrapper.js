import {useEffect, useState} from "react";

import CommunicationContext from "../Contexts/CommunicationContext";
import PlayerCommunicationService from './../Services/PlayerCommunicationService';
import UserCredentialsStorage from "../Services/UserCredentialsStorage";


const CommunicationWrapper = ({children}) => {
    const [players, setPlayers] = useState({});
    const [boardData, setBoardData] = useState([]);
    const [turn, setTurn] = useState(null);
    const [roomName, setRoomName] = useState('');
    const [winner, setWinner] = useState(null);
    const [winnerCellIndeces, setWinnerCellIndeces] = useState([]);

    useEffect(() => {
        PlayerCommunicationService.onRoomData((roomData) => {
            setPlayers(roomData.players);
            setBoardData(roomData.boardData);
            setTurn(roomData.turn);
            setRoomName(roomData.roomName);
            setWinner(roomData.winner);
            setWinnerCellIndeces(roomData.winnerCellIndeces || []);
        })
        PlayerCommunicationService.onStepAndSwitchTurn(({boardData, turn}) => {
            setBoardData(boardData);
            setTurn(turn);
        })
        PlayerCommunicationService.onGameEnd(({boardData, winner, winnerCellIndeces}) => {
            setBoardData(boardData);
            setWinner(winner);
            setWinnerCellIndeces(winnerCellIndeces);
        })
        PlayerCommunicationService.onRoomRemove(({roomName}) => {
            UserCredentialsStorage.removeRoomName();
            setPlayers({});
            setBoardData([]);
            setTurn(null);
            setRoomName(null);
            setWinner(null);
            setWinnerCellIndeces([]);
        })

        const roomName = UserCredentialsStorage.getRoomName();
        const myName = UserCredentialsStorage.getMyName();
        if (roomName && myName) {
            PlayerCommunicationService.joinToRoom(roomName, myName);
        }

    }, []);

    return <CommunicationContext.Provider value={{players, turn, boardData, roomName, winner, winnerCellIndeces}}>
        {children}
    </CommunicationContext.Provider>
}

export default CommunicationWrapper;