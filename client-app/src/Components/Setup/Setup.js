import {useState} from "react";
import {useHistory} from "react-router-dom";

import './Setup.css';
import TextInput from "../Common/TextInput/TextInput";
import Button from "../Common/Button/Button";
import PlayerCommunicationService from "../../Services/PlayerCommunicationService";
import UserCredentialsStorage from "../../Services/UserCredentialsStorage";

const Setup = () => {
    const [roomName, setRoomName] = useState(UserCredentialsStorage.getRoomName() || '');
    const [myName, setMyName] = useState(UserCredentialsStorage.getMyName() || '');
    const history = useHistory();

    const handleSubmit = async (e) => {
        e.preventDefault();

        await PlayerCommunicationService.joinToRoom(roomName, myName);
        UserCredentialsStorage.setMyName(myName);
        UserCredentialsStorage.setRoomName(roomName);

        history.push('/game');
    }

    return <div className={'Setup'}>
        <div className={'Setup__Inner'}>
            <form onSubmit={handleSubmit}>
                <div>
                    <h2>Join Room</h2>
                </div>

                <div className={'Form__Control'}>
                    <label htmlFor="myName">Enter Your Name</label>
                    <TextInput
                        id={'myName'}
                        type={'text'}
                        value={myName}
                        onChange={({target}) => setMyName(target.value)}
                        required
                    />
                </div>
                <div className={'Form__Control'}>
                    <label htmlFor="roomName">Room Name</label>
                    <TextInput
                        id={'roomName'}
                        type={'text'}
                        value={roomName}
                        onChange={({target}) => setRoomName(target.value)}
                        required
                    />
                </div>

                <div className={'Form__Control'}>
                    <Button
                        fluid
                        type="submit"
                    >
                        Join
                    </Button>
                </div>
            </form>
        </div>

    </div>
}

export default Setup;