import PropTypes from 'prop-types';

import './Overlay.css';

const Overlay = ({
                     children,
                     show,
                     content
                 }) => {
    return <div className={'Overlay'}>
        {children}
        {show ? <div className={'Overlay__Content'}>
            {content}
        </div> : null}
    </div>
}

Overlay.propTypes = {
    children: PropTypes.node,
    show: PropTypes.bool,
    content: PropTypes.node,
}

export default Overlay;