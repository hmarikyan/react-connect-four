import PropTypes from 'prop-types';
import './TextInput.css';

const TextInput = ({
                       value,
                       onChange,
                       onBlur,

                       type,

                       className,
                       id,
                       required,
                   }) => {
    const classNames = ['TextInput'];
    if (className) {
        classNames.push(className)
    }

    return <input
        id={id}
        className={classNames.join(' ')}
        type={type}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        required={required}
    />
}

TextInput.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ]),
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func,

    type: PropTypes.string,
    className: PropTypes.string,
    id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    required: PropTypes.bool,
}

TextInput.defaultProps = {
    value: '',
    onBlur: () => null,
    className: '',
    id: '',
    type: 'text',
    required: false,
}

export default TextInput;