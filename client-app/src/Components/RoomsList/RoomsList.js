import {useCallback, useEffect, useState} from "react";

import './RoomsList.css';
import PlayerCommunicationService from "../../Services/PlayerCommunicationService";
import Button from "../Common/Button/Button";

const RoomsList = () => {
    const [roomsList, setRoomsList] = useState([]);

    const fetchRoomsList = useCallback(async () => {
        const rs = await PlayerCommunicationService.getRoomsList();
        setRoomsList(rs);
    }, []);

    const removeRoom = useCallback(async (roomName) => {
        await PlayerCommunicationService.removeRoom({roomName});
        fetchRoomsList();
    }, []);


    useEffect(() => {
        fetchRoomsList();
    }, []);


    return <div className={'RoomsList'}>
        <div>
            <h2>Rooms</h2>
        </div>

        <div>
            {roomsList.map(room => <div className={'RoomsList__RoomItem'} key={room.roomName}>
                <div className={'RoomsList__RoomItem__Title'}>
                    {room.roomName}
                </div>
                <div className={'RoomsList__RoomItem__Players'}>
                    <div className={'RoomsList__RoomItem__PlayersTitle'}>
                        Players
                    </div>
                    <div className={'RoomsList__RoomItem__PlayersList'}>
                        {Object.values(room.players).map(player => <div key={player.playerName}>
                            Name: {player.playerName}, Color: {player.color}
                        </div>)}
                    </div>
                </div>
                <div className={'RoomsList__RoomItem__Button'}>
                    <Button size={'small'} onClick={() => removeRoom(room.roomName)}>Remove room</Button>
                </div>
            </div>)}
        </div>
    </div>
}

export default RoomsList;