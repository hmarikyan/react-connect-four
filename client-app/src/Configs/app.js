export const CELLS_TO_CONNECT = 4;

export const CELL_CONFIG = {
    EMPTY: 0,
    YELLOW: 1,
    RED: 2,
};

export const TURNS = {
    YELLOW: 'YELLOW',
    RED: 'RED',
};