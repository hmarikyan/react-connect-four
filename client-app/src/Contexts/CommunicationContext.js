import React from "react";

export default React.createContext({players: [], turn: null, boardData: [], roomName: null});