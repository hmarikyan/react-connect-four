import {CELL_CONFIG, TURNS} from "../Configs/app";

export const getClassNameByCellValue = (cell) => {
    switch (cell) {
        case CELL_CONFIG.YELLOW: {
            return TURNS.YELLOW;
        }
        case CELL_CONFIG.RED: {
            return TURNS.RED;
        }
        default: {
            return 'EMPTY'
        }
    }
}

export const getAnimationDuration = (rowId, rowsCount) => {
    if(rowId < rowsCount / 3) {
        return 0.1;
    } else if ( rowId > rowsCount * 2 / 3){
        return 1;
    } else {
        return 0.5;
    }
}