import {io} from "socket.io-client";

import {SOCKET_URL} from "../Configs/communicationService";


class PlayerCommunicationService {
    #socket = null;

    init() {
        if (!this.#socket) {
            this.#socket = io(SOCKET_URL);
            this.#socket.on("connect", () => {
            });
        }
    }

    onRoomData(callbackFn) {
        this.#socket.on("room-data", (roomData) => {
            callbackFn(roomData);
        });
    }

    onStepAndSwitchTurn(callbackfn) {
        this.#socket.on("do-step", ({boardData, turn}) => {
            callbackfn({boardData, turn});
        });
    }

    onGameEnd(callbackfn) {
        this.#socket.on("end-game", ({boardData, winner, winnerCellIndeces}) => {
            callbackfn({boardData, winner, winnerCellIndeces});
        });
    }

    async onRoomRemove(callbackfn) {
        this.#socket.on("remove-room", ({roomName}) => {
            callbackfn({roomName});
        });
    }

    async joinToRoom(roomName, myName) {
        return new Promise((resolve, reject) => {
            this.#socket.emit(
                "request-join-room",
                {
                    roomName: roomName,
                    playerName: myName
                }, (response) => {
                    response ? resolve() : reject();
                });
        });
    }

    async doStepAndSwitchTurn({columnId, roomName}) {
        return new Promise((resolve, reject) => {
            this.#socket.emit(
                "request-do-step",
                {
                    roomName,
                    columnId,
                }
            );
            resolve();
        });
    }

    async resetGame({roomName}) {
        return new Promise((resolve, reject) => {
            this.#socket.emit(
                "request-reset-game",
                {
                    roomName,
                }
            );
            resolve();
        });
    }

    async leaveRoom({roomName, myName}) {
        return new Promise((resolve, reject) => {
            this.#socket.emit(
                "request-leave-room",
                {
                    roomName,
                    playerName: myName,
                }
            );

            resolve();
        });
    }

    async getRoomsList() {
        return new Promise((resolve, reject) => {
            this.#socket.emit(
                "request-rooms-list",
                (roomsList) => {
                    resolve(roomsList);
                }
            );
        });
    }

    async removeRoom({roomName}) {
        return new Promise((resolve, reject) => {
            this.#socket.emit(
                "request-remove-room",
                {
                    roomName,
                },
                () => resolve(),
            );
        });
    }
}


export default new PlayerCommunicationService();