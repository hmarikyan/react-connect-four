const UserCredentialsStorage = {
    setMyName: (myName) => localStorage.setItem('myName', myName),
    getMyName: () => localStorage.getItem('myName'),

    setRoomName: (roomName) => localStorage.setItem('roomName', roomName),
    getRoomName: () => localStorage.getItem('roomName'),
    removeRoomName: () => localStorage.removeItem('roomName'),
}

export default UserCredentialsStorage;